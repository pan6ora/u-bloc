# U-Bloc

This Web App is a fork of the project [RocoLib](https://github.com/javigallostra/RocoLib) maintain by Javi Gallostra.

This repos is a version of the app adapted to a specific usage for the Université Grenoble-Alpes climbing facilities. Il has also some UI tweaks that are personal decisions about what seems more intuitive to me.

This version can be seen at [bloc.u-grimpe.com](https://bloc.u-grimpe.com). The branch used in production is currently `dev`.

All credits for this cool work gets to him. Major changes and improvement will be helded in Github's RocoLib repos. For more infos about the Web App and how to contribute, go to the [RocoLib project page](https://github.com/javigallostra/RocoLib).
